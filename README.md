# Water Quality Measuring.

Water quality measuring with several sensors: PH, Total Dissolved Solids, Electroconductivity, Temperature, Turbity and Dissolved Oxygen. State of the current quality of seawater in mariculture and aquaculture. First stage with WiFi communication and then with LoRa.

### Context Diagram

<img src="/img/context-diagram.jpg" alt="Context Diagram" width="654" height="379">

| Component | Description |
| - | - |
| End Node | This set of devices are configured for sending and getting data from the LoRaWAN Network Server through the Gateway. There are basically a MCU that manage the energy, communication, data acquire and processing. The main board used in this project is: <br> **Heltec lora 32 v2** <br> Wich is a IoT development board, ESP32 based, Wifi, LoRa and Bluethoot compatible, with LED screen mounted. <br> In the other hand there are sensor that acquire different kind of measurements. The currently used **sensors** are: <br> - DS18B20 Temperature <br> - BMP180 Temperature and Pressure <br> **DFrobot** <br> - pH <br> - Electroconductivity <br> - Total Dissolved Solids <br> - Turbidity <br> **Atlas Scientific** <br> - pH <br> - Dissolved Oxygen<br> - Electroconductivity|
| Gateway | This device acquire the data from the end nodes and establish communication with the LoRaWAN network server, acting as a link between the two last. Until this point, the project has been developed with **Dragino Gateway LoRaWAN LG308 915MHz**.|
| Network Server | This server manages all the data received from end nodes. It may be through different gateways. Also the server manage the data through integration to an application.|
| Application | The application recive the data from the newtwork server and manages depending on the requirements of the user.|

## End Node Setup



## Components Configuration
|Component type|Image|Name|
|-|-|-|
| Endnode | <img src="/img/Heltec_1.png" alt="Heltec" width="200"> |[Heltec + Atlas Scientific + DFRobot](doc/setup.md) |
| Gateway | <img src="/img/Dragino.png" alt="Dragino" width="200"> |[Dragino LG308](doc/dragino.md) |
| Network Server | <img src="/img/TheThingsNetwork-logo-RGB.png" alt="ttn" width="200"> |[The Things Network](doc/ttn.md) |
| Application | <img src="/img/thingspeak-1200x750.png" alt="thingspeak" width="200"> |[Thingspeak](doc/thingspeak.md)|

