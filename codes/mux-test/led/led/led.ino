#include "heltec.h"
// set pin numbers
//const int buttonPin = 2;  // the number of the pushbutton pin
const int ledPin =  2;    // the number of the LED pin

// variable for storing the pushbutton status 
int buttonState = 0;

void setup() {
  Serial.begin(115200);  
  Heltec.begin(true, false, true);
  // initialize the pushbutton pin as an input
  //pinMode(buttonPin, INPUT);
  // initialize the LED pin as an output
  pinMode(ledPin, OUTPUT);
}

void loop() {
  digitalWrite(ledPin, HIGH);
  delay(2000);
  digitalWrite(ledPin, LOW);
  delay(2000);
}
