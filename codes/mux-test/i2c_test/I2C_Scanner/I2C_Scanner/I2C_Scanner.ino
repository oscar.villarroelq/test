/* Heltec Automation I2C scanner example (also it's a basic example how to use I2C1)
 *
 * ESP32 have two I2C (I2C0 and I2C1) bus
 *
 * OLED is connected to I2C0, so if scan with Wire (I2C0), the return address should be 0x3C.
 *
 * If you need scan other device address in I2C1...
 *		- Comment all Wire.***() codes;
 * 		- Uncomment all Wire1.***() codes;
 *
 * I2C scan example and I2C0
 *
 * HelTec AutoMation, Chengdu, China
 * 成都惠利特自动化科技有限公司
 * www.heltec.org
 *
 * this project also realess in GitHub:
 * https://github.com/HelTecAutomation/Heltec_ESP32
 * */

#include "Arduino.h"
#include "heltec.h"
#include <Wire.h>

byte code = 0;                   //used to hold the I2C response code.
char datachar[20];                //we make a 20 byte character array to hold incoming data from the pH circuit.
byte in_char = 0;                //used as a 1 byte buffer to store inbound bytes from the pH Circuit.
byte i = 0;                      //counter used for ph_data array.
float datafloat = 0;

void TCA9548A(uint8_t bus){
  Wire1.beginTransmission(0x70);  // TCA9548A address
  Wire1.write(1 << bus);          // send byte to select bus
  Wire1.endTransmission();
  Serial.println(bus);
}

void setup()
{
	Heltec.begin(true, false, true);
	//Wire.begin(SDA_OLED, SCL_OLED); //Scan OLED's I2C address via I2C0
	Wire1.begin(SDA, SCL);        //If there have other device on I2C1, scan the device address via I2C1
  Wire1.setClock(100000);
  pinMode(2, OUTPUT);
}

void loop()
{
	byte error, address;
	int nDevices;

	Serial.println("Scanning...");

	nDevices = 0;
	for(address = 1; address < 127; address++ )
	{
//		Wire.beginTransmission(address);
//		error = Wire.endTransmission();

		Wire1.beginTransmission(address);
		error = Wire1.endTransmission();

		if (error == 0)
		{
			Serial.print("I2C device found at address 0x");
			if (address<16)
			Serial.print("0");
			Serial.print(address,HEX);
			Serial.println("  !");
			nDevices++;
		}
		else if (error==4)
		{
			Serial.print("Unknown error at address 0x");
			if (address<16)
				Serial.print("0");
			Serial.println(address,HEX);
		}
	}
	if (nDevices == 0)
	Serial.println("No I2C devices found\n");
	else
	Serial.println("done\n");

  TCA9548A(1);
  Wire1.beginTransmission(99);  // Transmit to device number 99 (0x63)
  Wire1.write('r');             // Sends value byte
  Wire1.endTransmission();      // Stop transmitting
  delay(800);
  Wire1.requestFrom(99,20,1);    // Request 6 bytes from slave device number two
  code = Wire1.read();   
  // Slave may send less than requested
  while (Wire1.available()) {             //are there bytes to receive.
    in_char = Wire1.read();              //receive a byte.
    datachar[i] = in_char;               //load this byte into our array.
    i += 1;                             //incur the counter for the array element.
  if (in_char == 0) {                 //if we see that we have been sent a null command.
    i = 0;                            //reset the counter i to 0.
    datafloat = atof(datachar);
    Serial.println(datachar);
    //Serial.println(datafloat);
    break;                            //exit the while loop.
    }
  }
  delay(100);
  TCA9548A(0);
  Wire1.beginTransmission(97);  // Transmit to device number 99 (0x63)
  Wire1.write('r');             // Sends value byte
  Wire1.endTransmission();      // Stop transmitting
  delay(800);
  Wire1.requestFrom(97,20,1);    // Request 6 bytes from slave device number two
  code = Wire1.read();   
  // Slave may send less than requested
  while (Wire1.available()) {             //are there bytes to receive.
    in_char = Wire1.read();              //receive a byte.
    datachar[i] = in_char;               //load this byte into our array.
    i += 1;                             //incur the counter for the array element.
  if (in_char == 0) {                 //if we see that we have been sent a null command.
    i = 0;                            //reset the counter i to 0.
    datafloat = atof(datachar);
    Serial.println(datachar);
    //Serial.println(datafloat);
    break;                            //exit the while loop.
    }
  }

	delay(2000);
}
