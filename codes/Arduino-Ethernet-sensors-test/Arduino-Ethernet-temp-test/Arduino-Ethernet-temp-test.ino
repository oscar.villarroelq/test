#include <SPI.h>
#include <Ethernet.h>
#include <OneWire.h>               // Sensor de temperatura 
#include <DallasTemperature.h>     //Sensor de temperatura
#include "DFRobot_PH.h"            // Sensor de Ph
#include "DFRobot_EC.h"            // Sensor de EC
#include "GravityTDS.h"            // Sensor de TDS

#define TurbidityPin A4
#define PH_PIN A5
#define EC_PIN A2
#define TdsSensorPin A3
#define temp_pin 2
#define time1 15000U // Tiempo entre mediciónes
#define EnableCalibration 0 // 1 para PH | 2 para EC | 0 para deshabilitar

#define TDSAdDress 20   // Dirección de memoria donde se almacena la K del Td #importante
#define center 0.444

//int pin = 0; // analog pin
//int tempc = 0,tempf=0; // temperature variables
//int samples[8]; // variables to make a better precision
//int maxi = -100,mini = 100; // to start max/min temperature
int i;
float Turbidityval,VoltTurbi; // Turbidity
float temperature = 20 ;      // Temp
float phValue,phVolt;         // PH
float tdsVolt2,tdsValue2;     // TDS por medio de EC.
float ecVoltage,ecValue;      // EC
float tdsValue = 0;           // TDS

// Local Network Settings
byte mac[]     = { 0xD4, 0xA8, 0xE2, 0xFE, 0xA0, 0xA1 }; // Must be unique on local network
byte ip[]      = { 10,2,50,73 };                // Must be unique on local network
byte gateway[] = { 10,2,50,1};
byte subnet[]  = { 255, 255, 255, 0 };

// ThingSpeak Settings
char thingSpeakAddress[] = "api.thingspeak.com";
String writeAPIKey = "45B6W8A6NVBV18XK";    // Write API Key for a ThingSpeak Channel
const int updateInterval = 10000;        // Time interval in milliseconds to update ThingSpeak   

// Variable Setup
long lastConnectionTime = 0; 
boolean lastConnected = false;
int failedCounter = 0;

// Initialize Arduino Ethernet Client
EthernetClient client;
OneWire ourWire(temp_pin);                //Se establece el temp_pin como bus OneWire (Sensor temperatura)
DallasTemperature tempsensor(&ourWire); //Se declara una variable u objeto para nuestro sensor  (Sensor temperatura)
DFRobot_PH ph;          // Se establece el sensor de ph
DFRobot_EC ec;          // Se establece el sensor de ec.
GravityTDS gravityTds;  //Se establece el sensor TDS

/*---------- Begin Prototipe funcion ----------------*/
void getTurbidity();  // mide y entrega el valor en pantalla de turbidez
void getPH();  // mide y entrega el valor en pantalla de PH
void getTemp(); // Medición de temperatura
void getEC(); // Medición de conductividad eléctrica
void getTDS(); // Medición de TDS
void tds_begin(); // Inicialización del sensor TDS
/*---------- End Prototipe funcion ----------------*/


void setup()
{
  tempsensor.begin();   //Se inicia el sensor de temperatura
  ph.begin();          // Se inicia el sensor de PH 
  ec.begin();          // Se inicia el sensor de EC
  tds_begin();          // Se inicia el sensor TDS  
  Serial.begin(9600);
  Ethernet.begin(mac, ip, gateway, subnet);
  delay(100);
  Serial.print("ETHERNET SHIELD ip  is     : ");
  Serial.println(Ethernet.localIP()); 
   // Start Ethernet on Arduino
  startEthernet();
}

void loop()
{ 
  //tempc = ( 5.0 * analogRead(pin) * 100.0) / 1024.0;
  getTemp();  // Obtención de valores de todos los sensores
  getTurbidity();
  getPH(); 
  getEC(); 
  getTDS();  // Se obtiene a través de la medición de PH
  //String analogPin0 = String(tempc);
  
  // Print Update Response to Serial Monitor
  
  while(client.available())
  {
    char c = client.read();
    Serial.print(c);
    delay(10);
  }
  
  // Disconnect from ThingSpeak
  if (!client.connected() && lastConnected)
  {
    Serial.println();
    Serial.println("...disconnected.");
    Serial.println();
    client.stop();
  }
  
  // Update ThingSpeak
  if(!client.connected() && (millis() - lastConnectionTime > updateInterval))
  {
    //updateThingSpeak("field1="+analogPin0);
    updateThingSpeak("field1="+String(temperature)+"&field2="+String(phValue)+"&field3="+String((ecValue*1000))+"&field4="+String(tdsValue2)+"&field5="+String(Turbidityval));
  }
  
  lastConnected = client.connected();
  
}

/*------------------Begin Funcions --------------------*/

void updateThingSpeak(String tsData)
{
  if (client.connect(thingSpeakAddress, 80))
  { 
    /*
    client.print("POST /update HTTP/1.1\n");
    client.print("Host: api.thingspeak.com\n");
    client.print("Connection: close\n");
    client.print("X-THINGSPEAKAPIKEY: "+writeAPIKey+"\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(tsData.length());
    client.print("\n\n");

    client.print(tsData);
    */

    // send HTTP request header
    client.println("GET /update?api_key=45B6W8A6NVBV18XK&"+ tsData + " HTTP/1.1");
    client.println("Host: " + String(thingSpeakAddress));
    client.println("Connection: close");
    client.println(); // end HTTP request header
    
    lastConnectionTime = millis();
    
    if (client.connected())
    {
      Serial.println("Connecting to ThingSpeak...");
      Serial.println();
      
      failedCounter = 0;
    }
    else
    {
      failedCounter++;
  
      Serial.println("Connection to ThingSpeak failed ("+String(failedCounter, DEC)+")");   
      Serial.println();
    }
    
  }
  else
  {
    failedCounter++;
    
    Serial.println("Connection to ThingSpeak Failed ("+String(failedCounter, DEC)+")");   
    Serial.println();
    
    lastConnectionTime = millis(); 
  }
}

void startEthernet()
{
  
  client.stop();

  Serial.println("Connecting Arduino to network...");
  Serial.println();  

  delay(100);
  
  // Connect to network amd obtain an IP address using DHCP
  if (Ethernet.begin(mac) == 0)
  {
    Serial.println("DHCP Failed, reset Arduino to try again");
    Serial.println();
  }
  else {
    Serial.println("Arduino connected to network using DHCP");
    Serial.println();
    Serial.println("Data being uploaded to THINGSPEAK Server.......");
    Serial.println();
  }
  
  delay(100);
}
void getTemp(){
  
   tempsensor.requestTemperatures();   //Se envía el comando para leer la temperatura
   temperature = tempsensor.getTempCByIndex(0); //Se obtiene la temperatura en ºC
   //Serial.print("Temperatura= ");
   //Serial.print(temperature);
   //Serial.println(" C");    
}

void getTurbidity(){
   // Se propone calibrar el sensor mediante una linealización cerca del punto de operación,
   // Calibrándolo por medio del sensor de TDS o Conductividad.
   VoltTurbi =((analogRead(TurbidityPin)/1024.0)*5.0)-center; // Voltaje leído
   //Serial.print("turbidity Volt: "); Serial.print(VoltTurbi,3);
   Turbidityval =(-1120.4*(VoltTurbi)*(VoltTurbi))+(5742.3*(VoltTurbi))-4352.9 ;
    Turbidityval =(-1120.4*(VoltTurbi)*(VoltTurbi))+(5742.3*(VoltTurbi))-5352.9 ;
   Serial.print("  Trubidity value: ");
   Serial.println( Turbidityval); 
}

void getPH(){
  
        phVolt = analogRead(PH_PIN)/1024.0*5000;  // read the voltage
        phValue = ph.readPH(phVolt,temperature);  // convert voltage to pH with temperature compensation
        Serial.print("pH:");
        Serial.println(phValue,2);
        //Serial.print(" PHvolt:");  Serial.println(phVolt);  // debug mediante voltaje
        if (EnableCalibration == 1){
            ph.calibration(phVolt,temperature);   // ACTIVAR PARA CALIBRAR by Serail CMD - Pasos en el readme
        }
}

void getEC(){

    ecVoltage = analogRead(EC_PIN)/1024.0*5000;   // read the voltage
    ecValue =  ec.readEC(ecVoltage,temperature);  // convert voltage to EC with temperature compensation
    tdsValue2 = (1000*ecValue)/2;  // TDS medición FACTOR 0,5
    Serial.print("EC:");
    Serial.print((ecValue*1000),2);
    Serial.println("us/cm");
    //Serial.print(ecValue,4);
    //Serial.println("ms/cm");
    Serial.print(tdsValue2,0);
    Serial.println(" ppm");
    
    if (EnableCalibration==2){
        ec.calibration(ecVoltage,temperature);   // ACTIVAR PARA CALIBRAR by Serail CMD - Pasos en el readme
    }
}

void getTDS(){
    gravityTds.setTemperature(temperature);  // set the temperature and execute temperature compensation
    gravityTds.update();  //sample and calculate
    tdsValue = gravityTds.getTdsValue();  // then get the value
    Serial.print(tdsValue,1);
    Serial.println(" ppm");
    if (EnableCalibration==1){
      //gravityTds.ONcalibrate();   // Función creada por C.R para habilitar y deshabilitar la calibración (fue modificada la librería)
                                       // ACTIVAR PARA CALIBRAR by Serail CMD - Pasos en el readme
    }
}

void tds_begin(){
    gravityTds.setPin(TdsSensorPin);
    gravityTds.setAref(5.0);  //reference voltage on ADC, default 5.0V on Arduino UNO
    gravityTds.setAdcRange(1024);  //1024 for 10bit ADC;4096 for 12bit ADC
    gravityTds.setKvalueAddress(TDSAdDress); // Dirección de memoria para guardar la constante K de calibración.
    gravityTds.begin();  //initialization
}
