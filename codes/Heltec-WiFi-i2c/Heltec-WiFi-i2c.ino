/*
  WriteMultipleFields
  
  Description: Writes values to fields 1,2,3,4 and status in a single ThingSpeak update every 20 seconds.
  
  Hardware: ESP32 based boards
  
  !!! IMPORTANT - Modify the secrets.h file for this project with your network connection and ThingSpeak channel details. !!!
  
  Note:
  - Requires installation of EPS32 core. See https://github.com/espressif/arduino-esp32/blob/master/docs/arduino-ide/boards_manager.md for details. 
  - Select the target hardware from the Tools->Board menu
  - This example is written for a network using WPA encryption. For WEP or WPA, change the WiFi.begin() call accordingly.
  
  ThingSpeak ( https://www.thingspeak.com ) is an analytic IoT platform service that allows you to aggregate, visualize, and 
  analyze live data streams in the cloud. Visit https://www.thingspeak.com to sign up for a free account and create a channel.  
  
  Documentation for the ThingSpeak Communication Library for Arduino is in the README.md folder where the library was installed.
  See https://www.mathworks.com/help/thingspeak/index.html for the full ThingSpeak documentation.
  
  For licensing information, see the accompanying license file.
  
  Copyright 2020, The MathWorks, Inc.
*/

#include "Arduino.h"
#include "heltec.h"
#include <Wire.h>
#include <WiFi.h>
#include "secrets.h"
#include "ThingSpeak.h" // always include thingspeak header file after other header files and custom macros

char ssid[] = SECRET_SSID;   // your network SSID (name) 
char pass[] = SECRET_PASS;   // your network password
int keyIndex = 0;            // your network key Index number (needed only for WEP)
WiFiClient  client;

byte code = 0;                   //used to hold the I2C response code.
char datachar[20];                //we make a 20 byte character array to hold incoming data from the pH circuit.
byte in_char = 0;                //used as a 1 byte buffer to store inbound bytes from the pH Circuit.
byte i = 0;                      //counter used for ph_data array.
float datafloat1 = 0;
float datafloat2 = 0;

unsigned long myChannelNumber = SECRET_CH_ID;
const char * myWriteAPIKey = SECRET_WRITE_APIKEY;

void TCA9548A(uint8_t bus){
  Wire1.beginTransmission(0x70);  // TCA9548A address
  Wire1.write(1 << bus);          // send byte to select bus
  Wire1.endTransmission();
  Serial.println(bus);
}

void setup() {
  Heltec.begin(true, false, true);
  Wire1.begin(SDA, SCL);        //If there have other device on I2C1, scan the device address via I2C1
  Wire1.setClock(100000);
  pinMode(2, OUTPUT);  
  Serial.begin(115200);  //Initialize serial
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo native USB port only
  }
  
  WiFi.mode(WIFI_STA);   
  ThingSpeak.begin(client);  // Initialize ThingSpeak
}

void loop() {
  TCA9548A(1);
  Wire1.beginTransmission(99);  // Transmit to device number 99 (0x63)
  Wire1.write('r');             // Sends value byte
  Wire1.endTransmission();      // Stop transmitting
  delay(800);
  Wire1.requestFrom(99,20,1);    // Request 6 bytes from slave device number two
  code = Wire1.read();   
  // Slave may send less than requested
  while (Wire1.available()) {             //are there bytes to receive.
    in_char = Wire1.read();              //receive a byte.
    datachar[i] = in_char;               //load this byte into our array.
    i += 1;                             //incur the counter for the array element.
  if (in_char == 0) {                 //if we see that we have been sent a null command.
    i = 0;                            //reset the counter i to 0.
    datafloat1 = atof(datachar);
    Serial.println(datachar);
    //Serial.println(datafloat);
    break;                            //exit the while loop.
    }
  }
  delay(100);
  TCA9548A(0);
  Wire1.beginTransmission(97);  // Transmit to device number 99 (0x63)
  Wire1.write('r');             // Sends value byte
  Wire1.endTransmission();      // Stop transmitting
  delay(800);
  Wire1.requestFrom(97,20,1);    // Request 6 bytes from slave device number two
  code = Wire1.read();   
  // Slave may send less than requested
  while (Wire1.available()) {             //are there bytes to receive.
    in_char = Wire1.read();              //receive a byte.
    datachar[i] = in_char;               //load this byte into our array.
    i += 1;                             //incur the counter for the array element.
  if (in_char == 0) {                 //if we see that we have been sent a null command.
    i = 0;                            //reset the counter i to 0.
    datafloat2 = atof(datachar);
    Serial.println(datachar);
    //Serial.println(datafloat);
    break;                            //exit the while loop.
    }
  }
  // Connect or reconnect to WiFi
  if(WiFi.status() != WL_CONNECTED){
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(SECRET_SSID);
    while(WiFi.status() != WL_CONNECTED){
      WiFi.begin(ssid, pass);  // Connect to WPA/WPA2 network. Change this line if using open or WEP network
      Serial.print(".");
      delay(5000);     
    } 
    Serial.println("\nConnected.");
  }

  // set the fields with the values
  ThingSpeak.setField(1, datafloat1);
  ThingSpeak.setField(2, datafloat2);

  // write to the ThingSpeak channel
  int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem updating channel. HTTP error code " + String(x));
  }
  
  // change the values

  delay(15000); // Wait 20 seconds to update the channel again
}
