/*El ejemplo envia la varible de Temperatura a ThingSpeak utilizando la placa ESP32*/ 
/****************************************
*Definir libreris, instancias y constantes
*para la conexion WIFI
****************************************/
#include <WiFi.h>
const char * ssid = "AndroidAP47A9"; // Ingrese su nombre red wifi
const char * pass = "123345678"; //Ingresa la contraseña de tu red
WiFiClient client;

/****************************************
*Definir librería, instancias y constantes
*para la conexión a ThingSpeak
****************************************/
#include "ThingSpeak.h"
unsigned long Channel_ID = 1779298; //Ingrese su numero de canal de ThingSpeak
const char * WriteAPIKey = "NG172WX69U9YW14Y"; //Ingrese su clave de API de escritura de canal

/****************************************
*Definir librería, instancias y constantes
*para del sensor DS18B20
****************************************/
#include <OneWire.h> 
#include <DallasTemperature.h>
OneWire ourWire(23); //Se establece el pin 4 del ESP32 para la lectura del sensor
DallasTemperature DS18B20(&ourWire); //Se declara una variable u objeto para el sensor
float tem;

/****************************************
*Definiciones para sensor Capacitive Soil Moisture
****************************************/
#define pin_soilCap 38
const int AirValue = 2737;   //you need to replace this value with Value_1
const int WaterValue = 940;  //you need to replace this value with Value_2
int soilMoistureValue;
int soilmoisturepercent;
int SoilHumidity;

/****************************************
*Definiciones para sensor BME280
****************************************/
#include <Wire.h>
#include <Adafruit_BME280.h>
#define SEALEVELPRESSURE_HPA (1013.25)
Adafruit_BME280 bme;
int AirHumidity;
int Altura;
float INTtemperature;
float Pressure;

/****************************************
*Definiciones para sensor GYML8511
****************************************/
#define UVOUT_PIN 37 //Output from the sensor
#define REF_3V3_PIN 36 //3.3V power on the Arduino board
int uvLevel;
int UVrefLevel;
float UVoutputVoltage; 
float uvIntensity; 

/****************************************
*Funciones principales
****************************************/

void setup()
{
Serial.begin(115200); //Iniciar el monitor serie
Wire.begin(4,15);//(SDA, SCL); // Para sensor BME280
if (!bme.begin(0x76)) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }
pinMode(UVOUT_PIN, INPUT);    // Para sensor GYML8511
pinMode(REF_3V3_PIN, INPUT);  // Para sensor GYML8511
DS18B20.begin(); //Se inicia el sensor DS18B20

Serial.println("Iniciando conexión Wifi"); //Imprimir mensaje de conexión a la red Wifi
WiFi.begin(ssid, pass); //Se inicia la conexión al Wifi
//Mientras se conecta imprimirá ...
while(WiFi.status() != WL_CONNECTED){
  delay(500);
  Serial.print(".");
}
//Ya que se estableció la conexión al Wifi se imprime conexión establecida
Serial.println("Conexion establecida");
ThingSpeak.begin(client); //Iniciar el servidor de ThingSpeak
}

void loop(){
leer_sensor_DS18B20(); //Función para lectura del sensor
getCapSoil(); // Lectura del sensor Capacitive Soil Moisture
getPressure(); // Lectura del sensor BME280
getUVSensor(); // Lectura del sensor GYML8511

delay(3000); 
//Signar en ThingSpeak al campo 1 la lectura de temperatura
ThingSpeak.setField(1, tem); 
ThingSpeak.setField(2, AirHumidity); 
ThingSpeak.setField(3, Pressure); 
ThingSpeak.setField(4, uvIntensity); 

//Transmitir los datos al sevidor ThingSpeak
ThingSpeak.writeFields(Channel_ID, WriteAPIKey);
Serial.println("Datos enviados a ThingSpeak");
delay(2000);
}

/****************************************
*Funciones 
****************************************/

//Función para leer el sensor y asignar los campos a ThingSpeak
void leer_sensor_DS18B20(){ 
  DS18B20.requestTemperatures(); //Se envía el comando para leer la temperatura
  tem= DS18B20.getTempCByIndex(0); //Se obtiene la temperatura en ºC
  Serial.print("Temperatura= ");
  Serial.print(tem);
  Serial.println(" °C");
}

//Funciones para leer el sensor de humedad

void getCapSoil(){
  SoilHumidity = CapSoil();  
  Serial.print("Soil Humidity : ");
  Serial.print(SoilHumidity);
  Serial.println("%");
}

int CapSoil(){
  soilMoistureValue = analogRead(pin_soilCap); 
  soilmoisturepercent = map(soilMoistureValue, AirValue, WaterValue, 0, 100);  
  if(soilmoisturepercent >= 100){
    return 100;
  }
  else if(soilmoisturepercent <=0){
    return 0;
  }
  else if(soilmoisturepercent >0 && soilmoisturepercent < 100){
    return soilmoisturepercent;
  }
}

//Función para leer el sensor de BME280

void getPressure(){
  INTtemperature = bme.readTemperature();
  Altura = (int )bme.readAltitude(SEALEVELPRESSURE_HPA);
  Pressure = bme.readPressure() / 100.0F ;
  AirHumidity =(int)bme.readHumidity();
/*  
  Serial.print("Air Temperature = ");
  Serial.print(INTtemperature);
  Serial.println(" [*C]");

  Serial.print("Pressure = ");
  Serial.print(Pressure);
  Serial.println(" [hPa]");

  Serial.print("Approx. Altitude = ");
  Serial.print( Altura );
  Serial.println(" [m]");

  Serial.print("Humidity = ");
  Serial.print( AirHumidity );
  Serial.println("%");
*/
  delay(100);
}

//Funciones para leer el sensor GYML8511

void getUVSensor(){
 uvLevel = averageAnalogRead(UVOUT_PIN);
 UVrefLevel = averageAnalogRead(REF_3V3_PIN);
 
 //Use the 3.3V power pin as a reference to get a very accurate output value from sensor
 UVoutputVoltage = 3.3 / UVrefLevel * uvLevel;  
 uvIntensity = mapfloat(UVoutputVoltage, 0.88, 2.9, 0.0, 15.0);
 if (uvIntensity<0){uvIntensity=0;}

 //Serial.println("UV Level= " + (String)uvLevel);
 Serial.println("Voltage= " + (String)UVoutputVoltage);
 Serial.println("UV = " + (String)uvIntensity + "[mW/cm^2]");
}

//Takes an average of readings on a given pin
//Returns the average
int averageAnalogRead(int pinToRead)
{
  byte numberOfReadings = 8;
  unsigned int runningValue = 0; 
  for(int x = 0 ; x < numberOfReadings ; x++)
    runningValue += analogRead(pinToRead);
    delay(100);
  runningValue /= numberOfReadings;
  return(runningValue);  
  
}
//The Arduino Map function but for floats
float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
