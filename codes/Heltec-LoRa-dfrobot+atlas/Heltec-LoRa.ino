// Includes
#include "heltec.h"
#include <Wire.h>
#include "DFRobot_ESP_EC.h"
#include "DFRobot_ESP_PH_WITH_ADC.h"
#include <EEPROM.h>
#include <OneWire.h>               // Sensor de temperatura 
#include <DallasTemperature.h>     //Sensor de temperatura
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <stdio.h>

// Defines

#define Vref_PIN 36 // 3v3
#define TURB_PIN 37
//#define TDS_PIN 38
#define PH_PIN 39 
#define EC_PIN 38
#define TEMP_PIN 23
#define t_muestras 1500    // 30000 = 30 segundos
#define TX_INTERVAL_t 30   // 300 segundos  = (5 min)

// Configuración LoRa

static const u1_t PROGMEM APPEUI[8]={ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}

// Debe estar en formato LSB
static const u1_t PROGMEM DEVEUI[8]={ 0x1D, 0x2D, 0x05, 0xD0, 0x7E, 0xD5, 0xB3, 0x70 };
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}

// Debe estar en formato MSB
static const u1_t PROGMEM APPKEY[16] = { 0xBE, 0x54, 0x51, 0xFC, 0xA7, 0xA0, 0x14, 0x2E, 0x8B, 0x18, 0x21, 0x82, 0xFD, 0xC0, 0xE9, 0x75 };
void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}

static osjob_t sendjob;

unsigned TX_INTERVAL = TX_INTERVAL_t;  //Schedule TX every this many seconds (might become longer due to duty cycle limitations). (t>20)

// Pin mapping
const lmic_pinmap lmic_pins = {
    .nss = 18,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 14,
    .dio = {26, 35, 34}, // Configuración Heltec LoRa32 V2.
};

// Variables locales
static uint8_t mydata[36]; //Mensaje a transmitir   (actualmente 31 carateres con presición 0.1)
int flag_TXCOMPLETE;
int conected=0;
float voltagePH,phValue;
float voltageEC,ecValue;
float voltageTURB,turbValue; // falta hacer turbiedad
float temperature;

// Declaraciones

DFRobot_ESP_EC ec;
DFRobot_ESP_PH_WITH_ADC ph;
OneWire ourWire(TEMP_PIN);                //Se establece el temp_pin como bus OneWire (Sensor temperatura)
DallasTemperature tempsensor(&ourWire); //Se declara una variable u objeto para nuestro sensor  (Sensor temperatura)

// Inicio de código

void setup() {
    Heltec.begin(true /*DisplayEnable Enable*/, true /*LoRa Enable*/, true /*Serial Enable*/);
    pinMode(13, OUTPUT);          //Led indicador
    Serial.begin(115200);       // Iniciamos la comunicación serial
    EEPROM.begin(32);//needed EEPROM.begin to store calibration k in eeprom
    ec.begin();
    ph.begin();
    tempsensor.begin();   //Se inicia el sensor de temperatura
    
    Heltec.begin(true /*DisplayEnable Enable*/, true /*LoRa Enable*/, true /*Serial Enable*/);
    delay(300);
    Heltec.display->clear();
    Heltec.display -> drawString(0, 0,"Hi!");
    Heltec.display -> display();

    Serial.println(F("Starting"));
    Serial.println("Starting");

    
    #ifdef VCC_ENABLE  // For Pinoccio Scout boards
    pinMode(VCC_ENABLE, OUTPUT);
    digitalWrite(VCC_ENABLE, HIGH);
    delay(1000);
    #endif
    
    os_init(); // LMIC init
    LMIC_reset(); // Reset the MAC state. Session and pending data transfers will be discarded.
    do_send(&sendjob);     // Start job (sending automatically starts OTAA too)

    delay(2000);
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0,"Connecting Dragino & TTN");
    Heltec.display -> drawString(0, 10," ( 3-4 minutes approx ) " );
    Heltec.display -> display();
}

void loop() {
  static unsigned long timepoint = millis();

  while(flag_TXCOMPLETE == 0)
  {
    os_runloop_once();
  }
  flag_TXCOMPLETE = 0;

  if(conected==0){
       conected=1;
       Heltec.display -> clear();
       Heltec.display -> drawString(0, 20,"CONNECTED!");
       Heltec.display -> drawString(0, 30,"measuring every X min");
       Heltec.display -> display();
       TX_INTERVAL=60; // original 300
  }

  if(millis()- timepoint > t_muestras){   //time interval 
      timepoint = millis();
      Serial.println("-------------------------------------------------");
      // Toma de muestras
      float Vref = analogRead(Vref_PIN); // 4096 app
      // Medición de temperatura
      tempsensor.requestTemperatures();   //Se envía el comando para leer la temperatura
      temperature = tempsensor.getTempCByIndex(0); //Se obtiene la temperatura en ºC
      
      // Medición de EC
      voltageEC = analogRead(EC_PIN)/Vref*5000;   // Se lee el voltaje en el pin
      ecValue =  ec.readEC(voltageEC,temperature);  // Se convierte el voltaje a EC compensando con la temperatura

      // Medición de PH
      voltagePH = analogRead(PH_PIN)/Vref*1000;  // Se lee el voltaje en el pin
      phValue = ph.readPH(voltagePH,temperature);  // Se convierte el voltaje a PH compensando con la temperatura
      
      // Medición de Turb
      int sensorValue = analogRead(TURB_PIN);   // Se lee el voltaje en el pin
      voltageTURB = sensorValue * (3.3 / 4096.0); // Convert the analog reading (which goes from 0 - 4096) to a voltage (0 - 3V3):
      turbValue =(-1120.4*(voltageTURB)*(voltageTURB))+(5742.3*(voltageTURB))-4352.9 ; // Función que convierte el voltaje medido a un valor de turbiedad

      // Se muestran los datos por pantalla en Heltec
      Heltec.display -> clear();
      Heltec.display -> drawString(0, 0,"T:    "+(String)(temperature));
      Heltec.display -> drawString(0,10,"EC:   "+(String)(ecValue));
      Heltec.display -> drawString(0,20,"pH:   "+(String)(phValue));
      Heltec.display -> drawString(0,30,"Turb: "+(String)(turbValue));
      //Heltec.display -> drawString(0,40,"UV = " + (String)uvIntensity + " [mW/cm^2]");  
      Heltec.display -> display();

      // Se juntan los datos a enviar
      sprintf((char*)mydata, "%.2f %.2f %.2f %.2f",temperature,ecValue,phValue,turbValue);    
      Serial.println((char*)mydata);
      Serial.print("len:");
      Serial.println(sizeof(mydata));     
      }
}

// Fin de código
