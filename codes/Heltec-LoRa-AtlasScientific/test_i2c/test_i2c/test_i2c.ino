//#include "Arduino.h"
#include "heltec.h"

byte code = 0;                   //used to hold the I2C response code.
char ph_data[20];                //we make a 20 byte character array to hold incoming data from the pH circuit.
byte in_char = 0;                //used as a 1 byte buffer to store inbound bytes from the pH Circuit.
byte i = 0;                      //counter used for ph_data array.


void setup() {
  Heltec.begin(true, false, true);
  Wire1.begin(21,22);
  Wire1.setClock(100000);
  Serial.begin(115200);
}

void loop() {
  Wire1.beginTransmission(99);  // Transmit to device number 99 (0x63)
  Wire1.write('r');             // Sends value byte
  Wire1.endTransmission();      // Stop transmitting

  delay(2000);

  Wire1.requestFrom(99,20,1);    // Request 6 bytes from slave device number two
  code = Wire1.read();                                                         //the first byte is the response code, we read this separately.

  switch (code) {                       //switch case based on what the response code is.
    case 1:                             //decimal 1.
      Serial.println("Success");        //means the command was successful.
      break;                            //exits the switch case.

    case 2:                             //decimal 2.
      Serial.println("Failed");         //means the command has failed.
      break;                            //exits the switch case.

    case 254:                           //decimal 254.
      Serial.println("Pending");        //means the command has not yet been finished calculating.
      break;                            //exits the switch case.

    case 255:                           //decimal 255.
      Serial.println("No Data");        //means there is no further data to send.
      break;                            //exits the switch case.
  }
  // Slave may send less than requested
  while (Wire1.available()) {             //are there bytes to receive.
    in_char = Wire1.read();              //receive a byte.
    ph_data[i] = in_char;               //load this byte into our array.
    i += 1;                             //incur the counter for the array element.
    if (in_char == 0) {                 //if we see that we have been sent a null command.
      i = 0;                            //reset the counter i to 0.
      break;                            //exit the while loop.
    }
  }
  
  Serial.println(ph_data);              //print the data.
  delay(3000);
}
