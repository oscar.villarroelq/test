
#include "heltec.h"
//#include "DFRobot_EC.h" // libería para Arduino
#include "DFRobot_ESP_EC.h"
//#include "DFRobot_PH.h" // librería para Arduino
#include "DFRobot_ESP_PH_WITH_ADC.h"
#include <EEPROM.h>
#include <OneWire.h>               // Sensor de temperatura 
#include <DallasTemperature.h>     //Sensor de temperatura

#define Vref_PIN 36 // 3v3
#define TURB_PIN 37
//#define TDS_PIN 38
#define PH_PIN 39 
#define EC_PIN 38
#define TEMP_PIN 23
float voltagePH,phValue;
float voltageEC,ecValue;
float voltageTURB,turbValue; // falta hacer turbiedad
float temperature = 21;
DFRobot_ESP_EC ec;
DFRobot_ESP_PH_WITH_ADC ph;
OneWire ourWire(TEMP_PIN);                //Se establece el temp_pin como bus OneWire (Sensor temperatura)
DallasTemperature tempsensor(&ourWire); //Se declara una variable u objeto para nuestro sensor  (Sensor temperatura)

void setup()
{
  Serial.begin(115200);  
  EEPROM.begin(32);//needed EEPROM.begin to store calibration k in eeprom
  ec.begin();
  ph.begin();
  tempsensor.begin();   //Se inicia el sensor de temperatura
}

void loop()
{
    static unsigned long timepoint = millis();
    if(millis()-timepoint>1000U)  //time interval: 1s
    {

      timepoint = millis();
      float Vref = analogRead(Vref_PIN); // 4096 app

      // Medición de temperatura
      tempsensor.requestTemperatures();   //Se envía el comando para leer la temperatura
      temperature = tempsensor.getTempCByIndex(0); //Se obtiene la temperatura en ºC
      
      // Medición de EC
      voltageEC = analogRead(EC_PIN)/Vref*5000;   // read the voltage
      ecValue =  ec.readEC(voltageEC,temperature);  // convert voltage to EC with temperature compensation

      // Medición de PH
      voltagePH = analogRead(PH_PIN)/Vref*1000;  // read the voltage
      phValue = ph.readPH(voltagePH,temperature);  // convert voltage to pH with temperature compensation
      
      // Medición de Turb
      int sensorValue = analogRead(TURB_PIN);// read the input on analog pin 0:
      voltageTURB = sensorValue * (3.3 / 4096.0); // Convert the analog reading (which goes from 0 - 4096) to a voltage (0 - 3V3):
      turbValue =(-1120.4*(voltageTURB)*(voltageTURB))+(5742.3*(voltageTURB))-4352.9 ;

      Serial.print("Temperatura: ");
      Serial.print(temperature,2);
      Serial.println(" °C ");
      Serial.print("EC: ");
      Serial.print(ecValue,2);
      Serial.println(" ms/cm ");
      Serial.print("pH: ");
      Serial.println(phValue,2);
      Serial.print("Turb: ");
      Serial.println(turbValue,2);

      
    }
    //ec.calibration(voltageEC,temperature);          // calibration process by Serail CMD
    //ph.calibration(voltage,temperature);           // calibration process by Serail CMD
}
