
/*
* @file  : Tx_Lora
* @author: Cristóbal E. Roldán S.
* @date  : 14/02/2022
* @brief : Transmisión mediante protocolo LORAWAN al servidor TTN,
* de 7 mediciónes: Temperatura interna y externa, Intensidad UV, humedad
* del aire, humedad de la tierra, presión barométrica y altura.
*/


/*----------Begin Includes----------------*/
#include "heltec.h"
#include <Adafruit_BME280.h>       //Sensor de presión
#include <Wire.h>
#include <OneWire.h>               // Sensor de temperatura 
#include <DallasTemperature.h>     //Sensor de temperatura
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <stdio.h>

/*----------end Includes----------------*/


// For normal use, we require that you edit the sketch to replace FILLMEIN
// with values assigned by the TTN console. However, for regression tests,
// we want to be able to compile these scripts. The regression tests define
// COMPILE_REGRESSION_TEST, and in that case we define FILLMEIN to a non-
// working but innocuous value.


#ifdef COMPILE_REGRESSION_TEST
# define FILLMEIN 0
#else
# warning "You must replace the values marked FILLMEIN with real values from the TTN control panel!"
# define FILLMEIN (#dont edit this, edit the lines that use FILLMEIN)
#endif

//------------------ DEFINES ------------------------------------------

#define temp_pin 23       //Sensor de temperatura
#define Motion_PIN 39    //Sensor de movimiento
#define pin_soilCap 38   //Sensor humedad suelo

#define UVOUT_PIN 37         //sensor UV 
#define REF_3V3_PIN 36       //3.3V power on the Arduino board

#define AirValue 3687    // CALIBRACIÓN SENSOR HUMEDAD TERRESTRE
#define WaterValue 1318  // CALIBRACIÓN SENSOR HUMEDAD TERRESTRE
#define SEALEVELPRESSURE_HPA (1013.25)  // CALIBRACIÓN SENSOR DE PRESIÓN
#define t_muestras 15000    // 30000 = 30 segundos
#define TX_INTERVAL_t 35 //300 segundos  = (5 min)
//-------------------------------------------------------------------



//---------------------------------LORA VALUES ----------------------------------------------------

// This EUI must be in lsb. For TTN issued EUIs the last bytes should be 0xD5, 0xB3,0x70.

static const u1_t PROGMEM APPEUI[8]={ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}

// This should also be in little endian format, see above.
static const u1_t PROGMEM DEVEUI[8]={ 0x4B, 0xCD, 0x04, 0xD0, 0x7E, 0xD5, 0xB3, 0x70};
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}

// This key should be in msb (it is a block of memory.
static const u1_t PROGMEM APPKEY[16] = { 0x57, 0xB2, 0xC3, 0xF4, 0x72, 0x9E, 0x64, 0x32, 0x9A, 0xEC, 0x1C, 0x30, 0x2E, 0xB9, 0xFF, 0xE3 };
void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}


static osjob_t sendjob;

unsigned TX_INTERVAL = TX_INTERVAL_t;  //Schedule TX every this many seconds (might become longer due to duty cycle limitations). (t>20)

// Pin mapping
const lmic_pinmap lmic_pins = {
    .nss = 18,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 14,
    .dio = {26, 35, 34},
};

//-------------------------- LORA VALUES -----------------------------------------------------------


/*---------- Begin local Variables ----------------*/

static uint8_t mydata[36]; //Mensaje a transmitir   (actualmente 31 carateres con presición 0.1)

int AUXsoilMoistureValue;   //Soil capacitive
int AUXsoilmoisturepercent; //Soil capacitive
int uvLevel;             //UV
int UVrefLevel;          //UV
int MotionVal;       // Motion
int SoilHumidity;
int AirHumidity;
int Altura;
int flag_TXCOMPLETE;
int conected=0;

float EXTtemperature = 20 ;      // Temp
float UVoutputVoltage;           //uv
float uvIntensity;               //uv
float INTtemperature;
float Pressure;


/*---------- End  definde and local Variables ----------------*/

/*------------ Declaraciónes -------------------------------------*/
OneWire oneWireObjeto(temp_pin);                  //Se establece el temp_pin como bus OneWire (Sensor temperatura)
DallasTemperature sensorDS18B20(&oneWireObjeto);  //Se declara una variable u objeto para nuestro sensor  (Sensor temperatura)
Adafruit_BME280 bme;
//Inicio sensor de presión

/*-------------- End Declaraciónes ------------------------------*/

/*---------- Begin Prototipe funcion ----------------*/
void getPressure();  // mide y entrega el valor en pantalla del sensor de presión, humedad, temperatura y altura
void getMotion();  // mide y entrega el valor en pantalla del sensor de movimiento
void getTemp(); // Medición de temperatura
void getCapSoil(); // Medición de humedad terrestre
int CapSoil();
void getUVSensor(); // Medición de UV

int averageAnalogRead(int pinToRead); //UV_funcion
float mapfloat(float x, float in_min, float in_max, float out_min, float out_max); //UV_funcion

/*---------- End Prototipe funcion ----------------*/



/*---------- begin code ----------------*/

void setup() {

    Heltec.begin(true /*DisplayEnable Enable*/, true /*LoRa Enable*/, true /*Serial Enable*/);
   
    pinMode(13, OUTPUT);          //Led indicador
    pinMode(UVOUT_PIN, INPUT);    //UV
    pinMode(REF_3V3_PIN, INPUT);  //UV
  
    Serial.begin(115200);       // Iniciamos la comunicación serial
    sensorDS18B20.begin();    //Se inicia el sensor de temperatura
  
   
    Heltec.begin(true /*DisplayEnable Enable*/, true /*LoRa Enable*/, true /*Serial Enable*/);
    delay(300);
    Heltec.display->clear();
    Heltec.display -> drawString(0, 0,"Hi!");
    Heltec.display -> display();

    Serial.println(F("Starting"));
    Serial.println("Starting");

    
    #ifdef VCC_ENABLE  // For Pinoccio Scout boards
    pinMode(VCC_ENABLE, OUTPUT);
    digitalWrite(VCC_ENABLE, HIGH);
    delay(1000);
    #endif
    
    os_init(); // LMIC init
    LMIC_reset(); // Reset the MAC state. Session and pending data transfers will be discarded.
    do_send(&sendjob);     // Start job (sending automatically starts OTAA too)

    delay(2000);
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0,"Connecting Dragino & TTN");
    Heltec.display -> drawString(0, 10," ( 3-4 minutes approx ) " );
    Heltec.display -> display();
}

void loop() {
  static unsigned long timepoint = millis();

  while(flag_TXCOMPLETE == 0)
  {
    os_runloop_once();
  }
  flag_TXCOMPLETE = 0;

  if(conected==0){
       conected=1;
       Heltec.display -> clear();
       Heltec.display -> drawString(0, 20,"CONNECTED!");
       Heltec.display -> drawString(0, 30,"measuring every 10 min");
       Heltec.display -> display();
       TX_INTERVAL=300;
  }

    if(millis()-timepoint>t_muestras){   //time interval 
        timepoint = millis();
        
        if (!bme.begin(0x76)) { // No se puede establecer conexión con el sensor de presión
            Serial.println("Could not find a valid BME280 sensor, check wiring!");
        }
        getTemp();  // Obtención de valores de todos los sensores
        getPressure();
        //getMotion(); 
        getCapSoil();     
        getUVSensor(); 
        Serial.println("-------------------------------------------------");

        Heltec.display -> clear();
        Heltec.display -> drawString(0, 0,"T  °C| air: "+(String)(INTtemperature)+" soil: "+(String)(EXTtemperature));
        Heltec.display -> drawString(0,10,"Hum %| air: "+(String)(AirHumidity)+" soil: "+(String)(SoilHumidity));
        Heltec.display -> drawString(0,20,"Press: "+(String)(Pressure));
       // Heltec.display -> drawString(0,30,"Mov: "+(String)(MotionVal));
        Heltec.display -> drawString(0,30,"High: "+(String)(Altura));
        Heltec.display -> drawString(0,40,"UV = " + (String)uvIntensity + " [mW/cm^2]");
        
        Heltec.display -> display();
        
        sprintf((char*)mydata, "%.1f %.1f %i %i %.1f %i %.3f",INTtemperature,EXTtemperature,SoilHumidity,AirHumidity,Pressure,Altura,uvIntensity);    
        Serial.println((char*)mydata);
        Serial.print("len:");
        Serial.println(sizeof(mydata)); 
      }
}
/*---------- End code ----------------*/




/*------------------Begin Funcions --------------------*/

void getPressure(){

  INTtemperature = bme.readTemperature();
  Altura = (int )bme.readAltitude(SEALEVELPRESSURE_HPA);
  Pressure = bme.readPressure() / 100.0F ;
  AirHumidity =(int)bme.readHumidity();
  Serial.print("Air Temperature = ");
  Serial.print(INTtemperature);
  Serial.println(" [*C]");

  Serial.print("Pressure = ");
  Serial.print(Pressure);
  Serial.println(" [hPa]");

  Serial.print("Approx. Altitude = ");
  Serial.print( Altura );
  Serial.println(" [m]");

  Serial.print("Humidity = ");
  Serial.print( AirHumidity );
  Serial.println("%");

  delay(100);

}

void getMotion(){

   MotionVal = analogRead(Motion_PIN);
   Serial.print("Analog Motion: "); // OJO CON LOS TIEMPOS
   Serial.println(MotionVal);

}

void getTemp(){
  
   sensorDS18B20.requestTemperatures();   //Se envía el comando para leer la temperatura
   EXTtemperature = sensorDS18B20.getTempCByIndex(0); //Se obtiene la temperatura en ºC
   Serial.print("Soil Temperature= ");
   Serial.print( EXTtemperature );
   Serial.println(" C");  
   
}

void getCapSoil(){

  SoilHumidity = CapSoil();
  
  Serial.print("Soil Humidity : ");
  Serial.print(SoilHumidity);
  Serial.println("%");
}

void getUVSensor(){
 uvLevel = averageAnalogRead(UVOUT_PIN);
 UVrefLevel = averageAnalogRead(REF_3V3_PIN);
 
 //Use the 3.3V power pin as a reference to get a very accurate output value from sensor
 UVoutputVoltage = 3.3 / UVrefLevel * uvLevel;  
 uvIntensity = mapfloat(UVoutputVoltage, 0.99, 2.9, 0.0, 15.0);
 if (uvIntensity<0){uvIntensity=0;}

 //Serial.println("UV Level= " + (String)uvLevel);
 //Serial.println("Voltage= " + (String)UVoutputVoltage);
 Serial.println("UV = " + (String)uvIntensity + "[mW/cm^2]");
}



int averageAnalogRead(int pinToRead)
{
  byte numberOfReadings = 8;
  unsigned int runningValue = 0; 
  for(int x = 0 ; x < numberOfReadings ; x++)
    runningValue += analogRead(pinToRead);
    delay(100);
  runningValue /= numberOfReadings;
  return(runningValue);  
  
}


//The Arduino Map function but for floats
float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}



int CapSoil(){
  AUXsoilMoistureValue = analogRead(pin_soilCap); 
  AUXsoilmoisturepercent = map(AUXsoilMoistureValue, AirValue, WaterValue, 0, 100);
  
  if(AUXsoilmoisturepercent >= 100){
    return 100;
  }
  else if(AUXsoilmoisturepercent <=0){
    return 0;
  }
  else if(AUXsoilmoisturepercent >0 && AUXsoilmoisturepercent < 100){
    return AUXsoilmoisturepercent;
  }
}



/*------------------End Funcions --------------------*/
