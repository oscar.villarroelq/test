#include "Arduino.h"
#include "heltec.h"
#include <Wire.h>
#include <WiFi.h>
#include "secrets.h"
#include "ThingSpeak.h" // always include thingspeak header file after other header files and custom macros

#define uS_TO_S_FACTOR 1000000ULL  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  15        /* Time ESP32 will go to sleep (in seconds) */

RTC_DATA_ATTR int bootCount = 0;

char ssid[] = SECRET_SSID;   // your network SSID (name) 
char pass[] = SECRET_PASS;   // your network password
int keyIndex = 0;            // your network key Index number (needed only for WEP)
WiFiClient  client;

byte code = 0;                   //used to hold the I2C response code.
char datachar[20];                //we make a 20 byte character array to hold incoming data from the pH circuit.
byte in_char = 0;                //used as a 1 byte buffer to store inbound bytes from the pH Circuit.
byte i = 0;                      //counter used for ph_data array.
float datafloat1 = 0;
float datafloat2 = 0;

unsigned long myChannelNumber = SECRET_CH_ID;
const char * myWriteAPIKey = SECRET_WRITE_APIKEY;

void TCA9548A(uint8_t bus){
  Wire1.beginTransmission(0x70);  // TCA9548A address
  Wire1.write(1 << bus);          // send byte to select bus
  Wire1.endTransmission();
  Serial.println(bus);
}

void setup(){
  Heltec.begin(true, false, true);
  Wire1.begin(SDA, SCL);        //If there have other device on I2C1, scan the device address via I2C1
  Wire1.setClock(100000);
  //pinMode(2, OUTPUT); 
  Serial.begin(115200);
  delay(1000); //Take some time to open up the Serial Monitor

  WiFi.mode(WIFI_STA);   
  ThingSpeak.begin(client);  // Initialize ThingSpeak
  TCA9548A(1);
  Wire1.beginTransmission(99);  // Transmit to device number 99 (0x63)
  Wire1.write('r');             // Sends value byte
  Wire1.endTransmission();      // Stop transmitting
  delay(800);
  Wire1.requestFrom(99,20,1);    // Request 6 bytes from slave device number two
  code = Wire1.read();   
  // Slave may send less than requested
  while (Wire1.available()) {             //are there bytes to receive.
    in_char = Wire1.read();              //receive a byte.
    datachar[i] = in_char;               //load this byte into our array.
    i += 1;                             //incur the counter for the array element.
  if (in_char == 0) {                 //if we see that we have been sent a null command.
    i = 0;                            //reset the counter i to 0.
    datafloat1 = atof(datachar);
    Serial.println(datachar);
    //Serial.println(datafloat);
    break;                            //exit the while loop.
    }
  }
  delay(100);
  TCA9548A(0);
  Wire1.beginTransmission(97);  // Transmit to device number 99 (0x63)
  Wire1.write('r');             // Sends value byte
  Wire1.endTransmission();      // Stop transmitting
  delay(800);
  Wire1.requestFrom(97,20,1);    // Request 6 bytes from slave device number two
  code = Wire1.read();   
  // Slave may send less than requested
  while (Wire1.available()) {             //are there bytes to receive.
    in_char = Wire1.read();              //receive a byte.
    datachar[i] = in_char;               //load this byte into our array.
    i += 1;                             //incur the counter for the array element.
  if (in_char == 0) {                 //if we see that we have been sent a null command.
    i = 0;                            //reset the counter i to 0.
    datafloat2 = atof(datachar);
    Serial.println(datachar);
    //Serial.println(datafloat);
    break;                            //exit the while loop.
    }
  }
  // Connect or reconnect to WiFi
  if(WiFi.status() != WL_CONNECTED){
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(SECRET_SSID);
    while(WiFi.status() != WL_CONNECTED){
      WiFi.begin(ssid, pass);  // Connect to WPA/WPA2 network. Change this line if using open or WEP network
      Serial.print(".");
      delay(5000);     
    } 
    Serial.println("\nConnected.");
  }

  // set the fields with the values
  ThingSpeak.setField(1, datafloat1);
  ThingSpeak.setField(2, datafloat2);

  // write to the ThingSpeak channel
  int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem updating channel. HTTP error code " + String(x));
  }

  //Increment boot number and print it every reboot
  ++bootCount;
  Serial.println("Boot number: " + String(bootCount));

  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  Serial.flush(); 
  esp_deep_sleep_start();
}

void loop(){
  //This is not going to be called
}
