#include <SPI.h>
#include <Ethernet.h>
#include <OneWire.h>               // Sensor de temperatura 
#include <DallasTemperature.h>     //Sensor de temperatura

#define temp_pin 2
//int pin = 0; // analog pin
//int tempc = 0,tempf=0; // temperature variables
//int samples[8]; // variables to make a better precision
//int maxi = -100,mini = 100; // to start max/min temperature
int i;
float temperature = 20 ;      // Temp

// Local Network Settings
byte mac[]     = { 0xD4, 0xA8, 0xE2, 0xFE, 0xA0, 0xA1 }; // Must be unique on local network
byte ip[]      = { 10,2,50,73 };                // Must be unique on local network
byte gateway[] = { 10,2,50,1};
byte subnet[]  = { 255, 255, 255, 0 };

// ThingSpeak Settings
char thingSpeakAddress[] = "api.thingspeak.com";
String writeAPIKey = "YL77U88BTFJGNUNV";    // Write API Key for a ThingSpeak Channel
const int updateInterval = 2000;        // Time interval in milliseconds to update ThingSpeak   

// Variable Setup
long lastConnectionTime = 0; 
boolean lastConnected = false;
int failedCounter = 0;

// Initialize Arduino Ethernet Client
EthernetClient client;
OneWire ourWire(temp_pin);                //Se establece el temp_pin como bus OneWire (Sensor temperatura)
DallasTemperature tempsensor(&ourWire); //Se declara una variable u objeto para nuestro sensor  (Sensor temperatura)

void setup()
{
  tempsensor.begin();   //Se inicia el sensor de temperatura
  Serial.begin(9600);
  Ethernet.begin(mac, ip, gateway, subnet);
  delay(100);
  Serial.print("ETHERNET SHIELD ip  is     : ");
  Serial.println(Ethernet.localIP()); 
   // Start Ethernet on Arduino
  startEthernet();
}

void loop()
{ 
  //tempc = ( 5.0 * analogRead(pin) * 100.0) / 1024.0;
  getTemp();  // Obtención de valores de todos los sensores

  //String analogPin0 = String(tempc);
  
  // Print Update Response to Serial Monitor
  
  while(client.available())
  {
    char c = client.read();
    Serial.print(c);
    delay(5);
  }
  
  // Disconnect from ThingSpeak
  if (!client.connected() && lastConnected)
  {
    Serial.println();
    Serial.println("...disconnected.");
    Serial.println();
    client.stop();
  }
  
  // Update ThingSpeak
  if(!client.connected() && (millis() - lastConnectionTime > updateInterval))
  {
    //updateThingSpeak("field1="+analogPin0);
    updateThingSpeak("field1="+String(temperature));
  }
  
  lastConnected = client.connected();
  
}

void updateThingSpeak(String tsData)
{
  if (client.connect(thingSpeakAddress, 80))
  { 
    /*
    client.print("POST /update HTTP/1.1\n");
    client.print("Host: api.thingspeak.com\n");
    client.print("Connection: close\n");
    client.print("X-THINGSPEAKAPIKEY: "+writeAPIKey+"\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(tsData.length());
    client.print("\n\n");

    client.print(tsData);
    */

    // send HTTP request header
    client.println("GET /update?api_key=YL77U88BTFJGNUNV&"+ tsData + " HTTP/1.1");
    client.println("Host: " + String(thingSpeakAddress));
    client.println("Connection: close");
    client.println(); // end HTTP request header
    
    lastConnectionTime = millis();
    
    if (client.connected())
    {
      Serial.println("Connecting to ThingSpeak...");
      Serial.println();
      
      failedCounter = 0;
    }
    else
    {
      failedCounter++;
  
      Serial.println("Connection to ThingSpeak failed ("+String(failedCounter, DEC)+")");   
      Serial.println();
    }
    
  }
  else
  {
    failedCounter++;
    
    Serial.println("Connection to ThingSpeak Failed ("+String(failedCounter, DEC)+")");   
    Serial.println();
    
    lastConnectionTime = millis(); 
  }
}

void startEthernet()
{
  
  client.stop();

  Serial.println("Connecting Arduino to network...");
  Serial.println();  

  delay(100);
  
  // Connect to network amd obtain an IP address using DHCP
  if (Ethernet.begin(mac) == 0)
  {
    Serial.println("DHCP Failed, reset Arduino to try again");
    Serial.println();
  }
  else {
    Serial.println("Arduino connected to network using DHCP");
    Serial.println();
    Serial.println("Data being uploaded to THINGSPEAK Server.......");
    Serial.println();
  }
  
  delay(100);
}
void getTemp(){
  
   tempsensor.requestTemperatures();   //Se envía el comando para leer la temperatura
   temperature = tempsensor.getTempCByIndex(0); //Se obtiene la temperatura en ºC
   //Serial.print("Temperatura= ");
   //Serial.print(temperature);
   //Serial.println(" C");    
}
