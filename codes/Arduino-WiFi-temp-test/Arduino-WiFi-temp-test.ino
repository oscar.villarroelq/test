#include <SoftwareSerial.h>
#include <OneWire.h>               // Sensor de temperatura 
#include <DallasTemperature.h>     //Sensor de temperatura

//  Arduino pin 2 (RX) to ESP8266 TX
//  Arduino pin 3 to voltage divider then to ESP8266 RX
//  Connect GND from the Arduiono to GND on the ESP8266
//  Pull ESP8266 CH_PD HIGH

// When a command is entered in to the serial monitor on the computer 
// the Arduino will relay it to the ESP8266

#define temp_pin 7
int LEDPIN = 13;
int test = 7;
float temperature = 20 ;      // Temp

    //------------Wifi Settings ----------------------
    String AP = "AndroidAP47A9";       // AP NAME
    String PASS = "123345678"; // AP PASSWORD  
    String API = "YL77U88BTFJGNUNV";   // Write API KEY
    String HOST = "api.thingspeak.com";
    String PORT = "80";
    int countTrueCommand;
    int countTimeCommand; 
    boolean found = false; 
    //---------------- End Wifi Settings -------------
SoftwareSerial mySerial(2, 3);  //RX,TX
OneWire ourWire(temp_pin);                //Se establece el temp_pin como bus OneWire (Sensor temperatura)
DallasTemperature tempsensor(&ourWire); //Se declara una variable u objeto para nuestro sensor  (Sensor temperatura)

void setup() 
{
    pinMode(LEDPIN, OUTPUT);
    tempsensor.begin();   //Se inicia el sensor de temperatura
    Serial.begin(9600);     // communication with the host computer
    //while (!Serial)   { ; }
 
    // Start the software serial for communication with the ESP8266
    mySerial.begin(115200);  
    sendCommand("AT",5,"OK");
    sendCommand("AT+CWMODE=1",5,"OK");
    sendCommand("AT+CWJAP=\""+ AP +"\",\""+ PASS +"\"",20,"OK");  
    Serial.println("");
    Serial.println("Remember to to set Both NL & CR in the serial monitor.");
    Serial.println("Ready");
    Serial.println("");
}
 
void loop() 
{
    // listen for communication from the ESP8266 and then write it to the serial monitor
    if ( mySerial.available() )   {  Serial.write( mySerial.read() );  }
 
    // listen for user input and send it to the ESP8266
    if ( Serial.available() )       {  mySerial.write( Serial.read() );  }
    getTemp();  // Obtención de valores de todos los sensores
    wifi_send();
}
void getTemp(){
  
   tempsensor.requestTemperatures();   //Se envía el comando para leer la temperatura
   temperature = tempsensor.getTempCByIndex(0); //Se obtiene la temperatura en ºC
   Serial.print("Temperatura= ");
   Serial.print(temperature);
   Serial.println(" C");    
}
void wifi_send(){
 //temperature,phValue,ecValue,tdsValue2,Turbidityval. (en este orden)

 String getData = "GET /update?api_key="+ API +"&field1="+temperature;
 sendCommand("AT+CIPMUX=1",5,"OK");
 sendCommand("AT+CIPSTART=0,\"TCP\",\""+ HOST +"\","+ PORT,15,"OK");
 sendCommand("AT+CIPSEND=0," +String(getData.length()+4),10,">");
 mySerial.println(getData);
 countTrueCommand++;
 sendCommand("AT+CIPCLOSE=0",1,"OK");
 delay(500);
}

void sendCommand(String command, int maxTime, char readReplay[]) {
  Serial.print(countTrueCommand);
  Serial.print(". at command => ");
  Serial.print(command);
  Serial.print(" ");
  while(countTimeCommand < (maxTime*1))
  {
    mySerial.println(command);//at+cipsend
    if(mySerial.find(readReplay))//ok
    {
      found = true;
      break;
    }
  
    countTimeCommand++;
  }
  
  if(found == true)
  {
    Serial.println("Ok!");
    countTrueCommand++;
    countTimeCommand = 0;
  }
  
  if(found == false)
  {
    Serial.println("Fail");
    countTrueCommand = 0;
    countTimeCommand = 0;
  }
  
  found = false;
 }
