## Configuration and Wiring guide

### 1.  Load libraries

The following libraries are needed for the programming of the nodes.

- DFRobot ESP (PH y EC)
- LMIC
- OneWire
- DallasTemperature
- heltec

This libraries can be directly downloaded from the Library Manager included in the Arduino IDE.

<img src="/img/libraries.jpg" alt="Libraries" width="800">

### 2. Configure LMIC library

In the Arduino library folder, search for `MCCI_LoRaWAN_LMIC_library`, then project_config, and open the `lmic_project_config.h` file. 
You have to uncomment the variable related to the used band, as shown in the image below:

<img src="/img/LMIC-config.jpg" alt="LMIC" width="800">

### 3. Configure devices

**diagrama de conexiones

### 4. Program devices

Open the Arduino IDE with all the libraries mentioned before (you can click on Verify to see if there are missing libraries). 
Click `Upload` to program the device.

