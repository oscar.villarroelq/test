### 6. Configure The Things Network

#### Create account and login

First, you have to create an account at [The Things Network webpage](https://www.thethingsnetwork.org/), then login as the images below:

<img src="/img/ttn-login.jpg" alt="ttn-login" width="800">

Click on `Console`, and login to The Things Stack.

<img src="/img/ttn-login-2.jpg" alt="ttn-login-2" width="325" height="408">

Choose `North America 1` (nam1) cluster.

<img src="/img/ttn-cluster.jpg" alt="ttn-cluster" width="800">

#### Set gateway connection

To configure the gateway in TTN, click `gateways` tab, then `add gateway`. In the adding page, configure `Gateway Server address` and
`Frequency plan` as shown below:

<img src="/img/ttn-gateway.jpg" alt="ttn-gateway" width="800">

<img src="/img/ttn-gateway-config.jpg" alt="ttn-gateway-config" width="800">

#### Set aplications

To configure the aplications in TTN, click `applications` tab, then `add application`.

<img src="/img/ttn-application.jpg" alt="ttn-application" width="800">

Click `Add end device` and set the configurations as shown in the image.

<img src="/img/ttn-application-enddevice.jpg" alt="ttn-application-enddevice" width="800">

In the configuration page, you have to set the `Frequency Plan`, `LoRaWAN version` and `Regional parameters version`.
Also, set the `DevEUI`, `AppEUI` and `AppKey`, just clicking in `generate`.

<img src="/img/ttn-application-enddevice-config.jpg" alt="ttn-application-enddevice-config" width="800">

#### Set integration
In the applications project page, go to `Integrations`, then `Webhooks` and click `Add webhook`.  

<img src="/img/ttn-application-webhook.jpg" alt="ttn-application-webhook" width="800">

You must configure the webhook, with the data given by ThingSpeak. It is, `Channel ID` and `API Key`.

<img src="/img/ttn-application-webhook-thingspeakconfig.jpg" alt="ttn-application-webhook-thingspeakconfig" width="800">

Finally, you have to set the `payload formatter` in the uplink tab. Copy the code as shown below.

<img src="/img/ttn-application-payloadformatter.jpg" alt="ttn-application-payloadformatter" width="800">
