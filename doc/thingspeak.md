### 7. Contigure ThingSpeak

#### Account creation and login

Go to [ThingSpeak Webpage](https://www.thingspeak.com/), create an account, then login.

<img src="/img/ts-login.jpg" alt="ts-login" width="800">

Then, click `Channels`, `My Channels`, `New Channel`.

<img src="/img/ts-newchannel.jpg" alt="ts-newchannel" width="800">

#### Channel Settings

Give a name and mark the fields you want to write data in. 

<img src="/img/ts-newchannel2.jpg" alt="ts-newchannel2" width="800">

Finally, go to `API Keys` tab so you can get the data for other configurations.

<img src="/img/ts-ID-API.jpg" alt="ts-ID-API" width="800">
