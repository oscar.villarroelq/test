
### Configure Dragino Gateway 

Turn on the Dragino device. To configure it, connect it to Ethernet via cable or try Wifi.
Enter the interface via browser by [10.130.1.1/](http://10.130.1.1/), then enter the `username: root` and `password: dragino`

<img src="/img/Dragino-login.jpg" alt="Dragino-login" width="800">

Once in the home page, look for `LoRaWAN Configuration`, and set LoRaWAN Server configuration `Service Provider` and `Server Address`.
Also, you can get the **Gateway ID** wich is used to configure TTN.

<img src="/img/Dragino-lorawan-config.jpg" alt="Dragino-lorawan-config" width="800">

Then, in `LoRa Configuration` tab, set the Frequency plan and sub-band.

<img src="/img/Dragino-lora-config.jpg" alt="Dragino-lora-config" width="800">
